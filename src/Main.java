public class Main {
    public static void main(String[] args) {
       // Shape shape = (String s) -> display(s);
        Shape shape = (String s) -> {
            System.out.println(s);
            System.out.println("1");
            System.out.println("2");
            System.out.println(s);
        };
        shape.print("Hello bec");
        double area = shape.calculateArea(4, x -> (double) x * x);
        System.out.println("Area of square: " + area);
        area = shape.calculateArea(4, x -> Math.PI * x * x);
        System.out.println("Area of circle: " + area);
    }

    public static void display(String s) {
        System.out.println(s);
        System.out.println("1");
        System.out.println("2");
    }
}
