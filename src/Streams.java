import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streams {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,4,5,6);
        Stream<Integer> stream = list.stream();
        list = stream
                .map(x -> x*x)
                .filter(x -> x < 10)
                .collect(Collectors.toList());
        System.out.println(list);

    }


}
