import java.util.function.Function;

@FunctionalInterface
public interface Shape {
    void print(String s, int a, int c, double k);
    default double calculateArea(int x, Function<Integer,Double> function){
        return function.apply(x);

    }
}
