import java.util.function.BiFunction;
import java.util.function.Function;

public class FILAB2 {
    public static void main(String[] args) {


//        double squareArea = calculateArea(4, x -> (double) x * x);
//        System.out.println("Area of square: " + squareArea);
//        double circleArea = calculateArea(4, x -> Math.PI * x * x);
//        System.out.println("Area of circle: " + circleArea);


        //math
        int a = 10;
        int b = 5;
        int sum = mathCalculator(10, 5, (x,y) -> x + y);
        System.out.println("sum: " + sum);
        int diff = mathCalculator(10, 5, (x,y) -> x - y);
        System.out.println("diff: " + diff);


    }

    public static double calculateArea(int x, Function<Integer, Double> function) {
       return function.apply(x);
    }

    public static int mathCalculator(int x,int y, BiFunction<Integer, Integer, Integer> function) {
        return function.apply(x,y);
    }
}
